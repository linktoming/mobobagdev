//
//  Constants.h
//  MoboBag
//
//  Created by Mingming Wang on 5/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//
#import <Foundation/Foundation.h>

#ifndef MoboBag_Constants_h
#define MoboBag_Constants_h

/*
 For Parse
 */
#define classProduct @"Product"

#define categoryFashionF @"FASHIONF"
#define categoryFashionM @"FASHIONM"
#define categoryGadgetAndAccessories @"GADGETANDACCESSORIES"
#define categoryBooks @"BOOKS"
#define categoryOthers @"OTHERS"

#define keyName @"name"
#define keyDetails @"details"
#define keyStoreLocation @"storeLocation"
#define keyContact @"contactNumber"
#define keyEmail @"email"
#define keyPrice @"price"
#define keyWeb @"web"
#define keyObjectId @"objectId"
#define keyCreatedAt @"createdAt"
#define keyUpdatedAt @"updatedAt"
#define keyFirstLineOfAddress @"firstLineOfAddress"
#define keySecondLineOfAddress @"secondLineOfAddress"

#define keyIsFeatured @"isFeatured"
#define keyDisplaySequence @"displaySequence"
#define keyMainImage @"productMainImage"
#define keyCategory @"category"


#define valueYes @"YES"
#define valueNo @"NO"

#define urlForUserFeedBack @"https://docs.google.com/spreadsheet/viewform?formkey=dENBR0M3cENVbmlsMjU0QW4zSHRIeUE6MQ#gid=0"
#define urlForMerchantInterestRegistration @"https://docs.google.com/spreadsheet/viewform?formkey=dHQ2T1Vpa1lxdTZ3bW12X1pEdnNvTXc6MQ#gid=0"

//For Flurry Tracking Event
#define eventBrowsed @"eventBrowsed"
#define eventShopped @"eventShopped"

#define eventVisit @"eventVisit"
#define event1Visit @"event1Visit"
#define event2Visit @"event2Visits"
#define event3to4Visits @"event3to4Visits"
#define event5AndAboveVisit @"event5AndAboveVisit"


///////////////////////////////////////////////////////////////////////////////
/**
 * This is the the DNS domain name of the endpoint your Token Vending
 * Machine is running.  (For example, if your TVM is running at
 * http://mytvm.elasticbeanstalk.com this parameter should be set to
 * mytvm.elasticbeanstalk.com.)
 */
#define TOKEN_VENDING_MACHINE_URL    @"tvmmobobagsample.elasticbeanstalk.com"

/**
 * This indiciates whether or not the TVM is supports SSL connections.
 */
#define USE_SSL                      NO


#define CREDENTIALS_ALERT_MESSAGE    @"Please update the Constants.h file with your credentials or Token Vending Machine URL."
#define ACCESS_KEY_ID                @"USED_ONLY_FOR_TESTING"  // Leave this value as is.
#define SECRET_KEY                   @"USED_ONLY_FOR_TESTING"  // Leave this value as is.

#define kNotificationForSESSuccess @"kNotificationForSESSuccess"
#define kNotificationForSESFail @"kNotificationForSESFail"

@interface Constants:NSObject {
}

+(UIAlertView *)credentialsAlert;
+(UIAlertView *)errorAlert:(NSString *)message;
+(UIAlertView *)expiredCredentialsAlert;
@end
#endif

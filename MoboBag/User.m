//
//  User.m
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import "User.h"

@implementation User
@synthesize name = _name;
@synthesize contactNumber = _contactNumber;
@synthesize email = _email;
@synthesize firstLineOfAddress = _firstLineOfAddress;
@synthesize secondLineOfAddress = _secondLineOfAddress;
- (NSString *)description{
    
    NSString *description = [NSString stringWithFormat:@"Name: %@\r\nContact Number: %@\r\nEmail: %@\r\nMailing Address: %@,%@",self.name, self.contactNumber,self.email,self.firstLineOfAddress,self.secondLineOfAddress];
    
    return description;
}
@end

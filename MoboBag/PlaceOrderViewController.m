//
//  PlaceOrderViewController.m
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import "PlaceOrderViewController.h"

@interface PlaceOrderViewController ()
- (void) sentSuccessfully:(NSNotification *)notification;
- (void) sentFailed:(NSNotification *)notification;
@end

@implementation PlaceOrderViewController
@synthesize order;
@synthesize name;
@synthesize contactNumber;
@synthesize email;
@synthesize firstLineOfAddress;
@synthesize secondLineOfAddress;
@synthesize processingIndicator;
@synthesize requestDelegate;
@synthesize detailViewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sentSuccessfully:) name:kNotificationForSESSuccess object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sentFailed:) name:kNotificationForSESFail object:nil];
    
    // Get default value for this order
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    self.name.text = [defaults valueForKey:keyName];
    self.contactNumber.text = [defaults valueForKey:keyContact];
    self.email.text = [defaults valueForKey:keyEmail];
    self.firstLineOfAddress.text = [defaults valueForKey:keyFirstLineOfAddress];
    self.secondLineOfAddress.text =[defaults valueForKey:keySecondLineOfAddress];
}

- (void)viewDidUnload
{
    [self setName:nil];
    [self setContactNumber:nil];
    [self setEmail:nil];
    [self setFirstLineOfAddress:nil];
    [self setSecondLineOfAddress:nil];
    [self setProcessingIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)clickedPlaceOrderButton:(UIButton *)sender {
    
    
    if ([self.name.text isEmpty]||[self.contactNumber.text isEmpty]
        ||[self.email.text isEmpty]||([self.firstLineOfAddress.text isEmpty]&&[self.secondLineOfAddress.text isEmpty])) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"All fields are required." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
    }else{
    
        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) registerEvent:eventShopped];
        
        self.order.user = [[User alloc] init];
        [self.order.user setName:self.name.text];
        [self.order.user setContactNumber:self.contactNumber.text];
        [self.order.user setEmail:self.email.text];
        [self.order.user setFirstLineOfAddress:self.firstLineOfAddress.text];
        [self.order.user setSecondLineOfAddress:self.secondLineOfAddress.text];
        
        // Set default value for next order and my info tab
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:self.name.text forKey:keyName];
        [defaults setValue:self.contactNumber.text forKey:keyContact];
        [defaults setValue:self.email.text forKey:keyEmail];
        [defaults setValue:self.firstLineOfAddress.text forKey:keyFirstLineOfAddress];
        [defaults setValue:self.secondLineOfAddress.text forKey:keySecondLineOfAddress];
        [defaults synchronize];
        
        SESSendEmailRequest * request = [[SESSendEmailRequest alloc] init];
        
        self.requestDelegate = [[SESRequestDelegate alloc] init];
        request.delegate = self.requestDelegate;
        
        [request setSource:@"orders@moboleague.com"];
        
        SESDestination *destination = [[SESDestination alloc] init];
        [destination addToAddresse:self.email.text];
        [destination addCcAddresse:[self.order.product valueForKey:keyEmail]];
        [destination addBccAddresse:@"orders+sent@moboleague.com"];
        [request setDestination:destination];
        
        SESMessage *message = [[SESMessage alloc] init];
        
        SESContent *subject = [[SESContent alloc] init];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
        [formatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        NSString *subjectString = [NSString stringWithFormat:@"Order Confirmation From MoboBag™ App on %@",        [formatter stringFromDate:[NSDate date]]];
        [subject setData:subjectString];
        [message setSubject:subject];
        
        SESBody *body = [[SESBody alloc] init];

        NSString *stringBody = [[NSString alloc] initWithFormat:@"Dear MoboBag™ App User:\r\n\r\nThank you for trying out MoboBag™ app.\r\nWe've received your order and forwarded it to the merchant by email as below:\r\n\r\n-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-\r\n%@\r\n-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-\r\n\r\nCurrently we are not processing any order for merchants.If the order is not processed promptly, please contact the merchant directly or feedback to us to this email.\r\n\r\nMoboBag™ app is in active development now and its service is surely not in full fledge.\r\nPlease feel free to give us any feedback or opinion about it here: http://bit.ly/NQYZmf We'll highly appreciate them which will shape the future version of the app.\r\n\r\nSincerely,\r\nMoboBag™ App Team",[self.order description]];
        SESContent *bodyText = [[SESContent alloc] init];
        [bodyText setData:stringBody];
        
        [body setText:bodyText];
        [message setBody:body];
        
        [request setMessage:message];

        
//        AmazonSESClient* sesClient = [[AmazonSESClient alloc] initWithAccessKey:@"AKIAIGYQVM44QEO5UP7Q" withSecretKey:@"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"];
//        [sesClient sendEmail:request];
        
        [self.processingIndicator startAnimating];
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [[AmazonClientManager ses] sendEmail:request];
        
    }
}

- (void) sentSuccessfully:(NSNotification *)notification{
    
    [self.processingIndicator stopAnimating];
    if([[UIApplication sharedApplication] isIgnoringInteractionEvents]){
        
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Your order was sent out successfully." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
    
    [[self navigationController] popToViewController:self.detailViewController animated:YES];
    
    
}
- (void) sentFailed:(NSNotification *)notification{
    
    
    [self.processingIndicator stopAnimating];
    if([[UIApplication sharedApplication] isIgnoringInteractionEvents]){
        
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"An err occured. Please check your network or email address." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
    
}
- (IBAction)didEndOnExitForNext:(UITextField *)sender {
    
    NSArray *textFieldArray = [NSArray arrayWithObjects:self.name,self.contactNumber,self.email,self.firstLineOfAddress,self.secondLineOfAddress, nil];
    
    if ([textFieldArray indexOfObject:sender]<[textFieldArray count]-1) {
        
        [[textFieldArray objectAtIndex:[textFieldArray indexOfObject:sender]+1] becomeFirstResponder];
    }else {
        
        [sender resignFirstResponder];
    }
}

@end

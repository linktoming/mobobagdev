//
//  Product.h
//  MoboBag
//
//  Created by Mingming Wang on 29/6/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic,assign) NSString *name;
@property (nonatomic,assign) NSString *description;
@property (nonatomic,assign) NSString *contact;
@property (nonatomic,assign) NSNumber *price;
@property (nonatomic,assign) UIImage *productMainImage;
@property (nonatomic,assign) UIImage *productSecondaryImage;
@property (nonatomic,assign) UIImage * productThumbnails;

- (id)initWithName: (NSString *)name description:(NSString *)description contact:(NSString *)contact
             price:(NSNumber *)price  productMainImage:(UIImage *)mainImage productSecondaryImage:(UIImage *)secondaryImage productThumbnails:(UIImage *)thumbnails;
@end

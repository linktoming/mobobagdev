//
//  Product.m
//  MoboBag
//
//  Created by Mingming Wang on 29/6/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize name = _name;
@synthesize description = _description;
@synthesize contact = _contact;
@synthesize price = _price;
@synthesize productMainImage = _productMainImage;
@synthesize productSecondaryImage = _productSecondaryImage;
@synthesize productThumbnails = _productThumbnails;

- (id)initWithName: (NSString *)name description:(NSString *)description contact:(NSString *)contact
             price:(NSNumber *)price  productMainImage:(UIImage *)mainImage productSecondaryImage:(UIImage *)secondaryImage productThumbnails:(UIImage *)thumbnails{

    self = [super init];
    if (self) {
        _name = name;
        _description = description;
        _contact = contact;
        _price = price;
        _productMainImage = mainImage;
        _productSecondaryImage = secondaryImage;
        _productThumbnails = thumbnails;

    }
    return self;

}
@end

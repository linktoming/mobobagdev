//
//  OrderPreferenceViewController.m
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import "OrderPreferenceViewController.h"

@interface OrderPreferenceViewController ()

@end

@implementation OrderPreferenceViewController
@synthesize order;
@synthesize product;
@synthesize isSelfCollection;
@synthesize isDeliveryService;
@synthesize isCashOnDelivery;
@synthesize isPayByPaypal;
@synthesize totalAmount;
@synthesize detailViewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.order = [[Order alloc] init];
    self.order.product = self.product;
    NSNumber *totalPrice = [NSNumber numberWithFloat:[((NSNumber *)[self.product valueForKey:keyPrice]) floatValue]+6];
    self.totalAmount.text =[NSString stringWithFormat:@"%@",totalPrice];

}

- (void)viewDidUnload
{
    [self setIsSelfCollection:nil];
    [self setIsDeliveryService:nil];
    [self setIsCashOnDelivery:nil];
    [self setIsPayByPaypal:nil];
    [self setTotalAmount:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    self.order.isSelfCollection = self.isSelfCollection.isOn;
    self.order.isDeliveryService = self.isDeliveryService.isOn;
    self.order.isCashOnDeliver = self.isCashOnDelivery.isOn;
    self.order.isPayByPaypal = self.isPayByPaypal.isOn;
    self.order.totalAmount = [NSNumber numberWithFloat:[self.totalAmount.text floatValue]];
    
    PlaceOrderViewController *controller = (PlaceOrderViewController *)segue.destinationViewController;
    controller.order = self.order;
    controller.detailViewController = self.detailViewController;

    
}
- (IBAction)changedDeliveryPreference:(UISwitch *)sender {
    
    if (sender.isOn) {
        NSNumber *totalPrice = [NSNumber numberWithFloat:[((NSNumber *)[self.product valueForKey:keyPrice]) floatValue]+6];
        self.totalAmount.text =[NSString stringWithFormat:@"%@",totalPrice];
    }else {
          self.totalAmount.text =[NSString stringWithFormat:@"%@",[self.product valueForKey:keyPrice]];
    }
    self.isSelfCollection.on = !self.isDeliveryService.isOn;

}

- (IBAction)changedSelfCollection:(UISwitch *)sender {
    
    self.isDeliveryService.on = !self.isSelfCollection.isOn;
    if (self.isDeliveryService.isOn) {
        NSNumber *totalPrice = [NSNumber numberWithFloat:[((NSNumber *)[self.product valueForKey:keyPrice]) floatValue]+6];
        self.totalAmount.text =[NSString stringWithFormat:@"%@",totalPrice];
    }else {
        self.totalAmount.text =[NSString stringWithFormat:@"%@",[self.product valueForKey:keyPrice]];
    }
}

- (IBAction)changedCashOnDelivery:(UISwitch *)sender {
    
    self.isPayByPaypal.on = !self.isCashOnDelivery.isOn;
}

- (IBAction)changedPayByPaypal:(UISwitch *)sender {
    
    self.isCashOnDelivery.on = !self.isPayByPaypal.on;
}
@end

//
//  MyInfoViewController.m
//  MoboBag
//
//  Created by Jacky Huang on 7/2/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import "MyInfoViewController.h"

@interface MyInfoViewController ()<UITableViewDelegate,UITableViewDataSource>


@end

@implementation MyInfoViewController
@synthesize nameLabel;
@synthesize contactNumberLabel;
@synthesize emailLabel;
@synthesize addressTextView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

}

- (void) viewWillAppear:(BOOL)animated{
    
    // Get default value for next order and my info tab
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    self.nameLabel.text = [defaults valueForKey:keyName];
    self.contactNumberLabel.text = [defaults valueForKey:keyContact];
    self.emailLabel.text = [defaults valueForKey:keyEmail];
    NSString *lineOne = [defaults valueForKey:keyFirstLineOfAddress];
    NSString *lineTwo = [defaults valueForKey:keySecondLineOfAddress];
    
    if (!lineOne&&!lineTwo) {
        self.addressTextView.text = @"";
    }else if (!lineTwo) {
        self.addressTextView.text = lineOne;
    }else if (!lineOne) {
        self.addressTextView.text = lineTwo;
    }else {
        self.addressTextView.text = [NSString stringWithFormat:@"%@,%@",lineOne ,lineTwo];
    }

}
- (void)viewDidUnload
{
    [self setNameLabel:nil];
    [self setContactNumberLabel:nil];
    [self setEmailLabel:nil];
    [self setAddressTextView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 3;
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NormalTrafficVideoViewControllerCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.textLabel.highlightedTextColor = [UIColor whiteColor];
        cell.detailTextLabel.highlightedTextColor = [UIColor whiteColor];
        cell.textLabel.numberOfLines = 0;
        
    }
    
    // Configure the cell... 
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Name:";
        cell.detailTextLabel.text = @"Bob";
    }else if (indexPath.row == 1) {
        cell.textLabel.text = @"Mail:";
        cell.detailTextLabel.text = @"bob@gmail.com";
    }else if (indexPath.row == 2) {
        cell.textLabel.text = @"Address:";
        cell.detailTextLabel.text = @"Leng Kee Road #06-07/08 SiS Building Singapore";
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end

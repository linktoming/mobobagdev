//
//  MyBagViewController.m
//  MoboBag
//
//  Created by Jacky Huang on 7/2/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import "MyBagViewController.h"

@interface MyBagViewController ()

@end

@implementation MyBagViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    MoboBagWebViewController *controller = (MoboBagWebViewController* )segue.destinationViewController;
    if (button.tag == 0) {
        
        controller.urlForFeedBack = urlForUserFeedBack;
    }else {
        controller.urlForFeedBack = urlForMerchantInterestRegistration;
    }
    
}
@end

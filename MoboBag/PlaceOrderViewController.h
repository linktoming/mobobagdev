//
//  PlaceOrderViewController.h
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "NSString+Utility.h"
#import "AmazonClientManager.h"
#import "SESRequestDelegate.h"
#import "AppDelegate.h"

@interface PlaceOrderViewController : UIViewController
@property (nonatomic,retain) Order *order;

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *contactNumber;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *firstLineOfAddress;
@property (weak, nonatomic) IBOutlet UITextField *secondLineOfAddress;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *processingIndicator;
@property (retain,nonatomic) SESRequestDelegate *requestDelegate;
@property (weak, nonatomic) UIViewController * detailViewController;

- (IBAction)clickedPlaceOrderButton:(UIButton *)sender;
- (IBAction)didEndOnExitForNext:(UITextField *)sender;

@end

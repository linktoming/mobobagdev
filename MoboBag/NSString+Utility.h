//
//  NSString+Utility.h
//  MoboBag
//
//  Created by Mingming Wang on 11/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

-(BOOL) isEmpty;
@end

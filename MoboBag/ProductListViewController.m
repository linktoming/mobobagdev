//
//  ProductListViewController.m
//  MoboBag
//
//  Created by Mingming Wang on 16/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import "ProductListViewController.h"

@interface ProductListViewController ()
- (void) getProductsInCategory: (NSString *)aCategory;
@end

@implementation ProductListViewController
@synthesize loadingIndicator;
@synthesize productListTableView;
@synthesize category,productArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.productListTableView.bounds.size.height, self.view.frame.size.width, self.productListTableView.bounds.size.height)];
		view.delegate = self;
		[self.productListTableView addSubview:view];
		_refreshHeaderView = view;
		
	}
	
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];
}

-(void)viewDidAppear:(BOOL)animated{
    
    if ([self.productArray count]==0) {
        [self getProductsInCategory:self.category];
    }
    
}
- (void)viewDidUnload
{
    [self setLoadingIndicator:nil];
    [self setProductListTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.productArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"productCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    PFObject *product = [self.productArray objectAtIndex:[indexPath row]];
    // Configure the cell...
    cell.textLabel.text = [product valueForKey:keyName];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"SGD %@",[product valueForKey:keyPrice]];
    
    PFFile *imageFile = [product valueForKey:keyMainImage];                    
    
    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        
        if (!error) {
            
            if (![self contentTypeForImageData:data]) {
                data = [data decompressGZip];
            }
            cell.imageView.frame = CGRectMake(0, 0, 106, 43);
            cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
            cell.imageView.image=[UIImage imageWithData:data];
            
            
        }else {
            NSLog(@"%@",[error description]);
        }
    }];

    
    return cell;
}

- (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

     [self performSegueWithIdentifier:@"gotoDetailFromProductList" sender:nil];
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
 
    [self performSegueWithIdentifier:@"gotoDetailFromProductList" sender:nil];
    
}
-(void) getProductsInCategory: (NSString *)aCategory{
    
    
    PFQuery *query = [PFQuery queryWithClassName:classProduct];
    [query whereKey:keyCategory equalTo:aCategory];
    [query orderByDescending:keyUpdatedAt];
//    query.limit = 100;
    query.cachePolicy=kPFCachePolicyCacheThenNetwork;
    [self.loadingIndicator startAnimating];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d products in %@", objects.count,aCategory);
            
            self.productArray = [[NSMutableArray alloc] initWithArray:objects];
                        
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);

        }
        [self.loadingIndicator stopAnimating];
        [self.productListTableView reloadData];
        
      [self doneLoadingTableViewData]; 
    }];
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"gotoDetailFromProductList"]) {
    
        PFObject *product = [self.productArray objectAtIndex: [self.productListTableView indexPathForSelectedRow].row];
        
        ((DetailViewController *)segue.destinationViewController).product = product;
    }
    
}


#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
    [self getProductsInCategory:self.category];
	_reloading = YES;
	
}

- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.productListTableView];
	
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == self.productListTableView) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
    if (scrollView == self.productListTableView) {
        
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    
	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
	
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}


@end
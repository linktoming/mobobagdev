//
//  ExploreViewController.h
//  MoboBag
//
//  Created by Mingming Wang on 5/7/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ProductListViewController.h"
@interface ExploreViewController : UITableViewController<UISearchDisplayDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *categoryTableView;
@property (strong,nonatomic) NSArray *categoryArray;
@property (strong,nonatomic) NSArray *categoryKeywordArray;

@end

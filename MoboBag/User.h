//
//  User.h
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic,assign) NSString *name;
@property (nonatomic,assign) NSString *contactNumber;
@property (nonatomic,assign) NSString *email;
@property (nonatomic,assign) NSString *firstLineOfAddress;
@property (nonatomic,assign) NSString *secondLineOfAddress;
@end

//
//  ProductListViewController.h
//  MoboBag
//
//  Created by Mingming Wang on 16/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Constants.h"
#import "DetailViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "NSData+GZip.h"

@interface ProductListViewController : UITableViewController<EGORefreshTableHeaderDelegate>{
    
    EGORefreshTableHeaderView *_refreshHeaderView;
	
	//  Reloading var should really be your tableviews datasource
	//  Putting it here for demo purposes 
	BOOL _reloading;
}
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

@property (nonatomic,assign) NSString *category;
@property (strong, nonatomic) NSMutableArray *productArray;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UITableView *productListTableView;
@end

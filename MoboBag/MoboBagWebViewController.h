//
//  MoboBagWebViewController.h
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoboBagWebViewController : UIViewController

@property (nonatomic, assign) NSString *urlForFeedBack;
@property (weak, nonatomic) IBOutlet UIWebView *feedBackWebView;
@end

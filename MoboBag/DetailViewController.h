//
//  DetailViewController.h
//  MoboBag
//
//  Created by Mingming Wang on 29/6/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Constants.h"
#import "OrderPreferenceViewController.h"
#import "Order.h"
#import "AppDelegate.h"
#import "NSData+GZip.h"
@interface DetailViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic,retain) PFObject *product;
@property (weak, nonatomic) IBOutlet UIScrollView *detailScrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *detailSegmentedControl;
- (IBAction)clickedDetailSegmentControl:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *detailImage;
@property (weak, nonatomic) IBOutlet UITextView *detailsTextView;
@property (weak, nonatomic) IBOutlet UITextView *contactTextView;
@end

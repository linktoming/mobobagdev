//
//  AppDelegate.m
//  MoboBag
//
//  Created by Mingming Wang on 5/7/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import "AppDelegate.h"
#import <AWSiOSSDK/AmazonLogger.h>
#import "Constants.h"
@implementation AppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    [Parse setApplicationId:@"ofxVJMUSFIaE468s7RfE1V72ferC2w8WDgV5XtOO"
                  clientKey:@"Pfxm02zGHsrGGso4JjIqpcOFpEnJx2FgfT6mkXlU"];
    
//    [PFQuery clearAllCachedResults];
    // Logging Control - Do NOT use logging for non-development builds.
#ifdef DEBUG
    [AmazonLogger verboseLogging];
#else
    [AmazonLogger turnLoggingOff];
    [Flurry startSession:@"PSK538Q3R767XJRRC8RS"];
#endif
    
    [self registerEvent:eventVisit];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self registerEvent:eventVisit];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(void)registerEvent:(NSString *)eventName{

    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if (![eventName isEqualToString:eventVisit]) {
        
        if(![defaults valueForKey:eventName]){
            [Flurry logEvent:eventName];
            [defaults setValue:eventName forKey:eventName];
        }
    }else {
        
        if ([defaults valueForKey:event5AndAboveVisit]) {
            
            // Do nothing, registed before
        }else if([defaults valueForKey:event3to4Visits]){
            
            // Visited 3 to 4 times before, this time is the 5 time
            [Flurry logEvent:event5AndAboveVisit];
            [defaults setValue:event5AndAboveVisit forKey:event5AndAboveVisit];
            
        }else if ([defaults valueForKey:event2Visit]) {
            
            [Flurry logEvent:event3to4Visits];
            [defaults setValue:event3to4Visits forKey:event3to4Visits];
            
        }else if ([defaults valueForKey:event1Visit]) {
            
            [Flurry logEvent:event2Visit];
            [defaults setValue:event2Visit forKey:event2Visit];
        }else {
            [Flurry logEvent:event1Visit];
            [defaults setValue:event1Visit forKey:event1Visit];
        }
    }
    
}
void uncaughtExceptionHandler(NSException *exception) {
    [Flurry logError:@"Uncaught" message:@"Crash!" exception:exception];
}
@end

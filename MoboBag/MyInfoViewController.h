//
//  MyInfoViewController.h
//  MoboBag
//
//  Created by Jacky Huang on 7/2/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
@interface MyInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextView *addressTextView;

@end

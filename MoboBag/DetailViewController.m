//
//  DetailViewController.m
//  MoboBag
//
//  Created by Mingming Wang on 29/6/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (nonatomic,strong) IBOutlet UIButton *buyButton;

@end

@implementation DetailViewController
@synthesize detailImage;
@synthesize detailsTextView;
@synthesize contactTextView;
@synthesize product;
@synthesize detailScrollView;
@synthesize detailSegmentedControl;
@synthesize buyButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = [self.product objectForKey:keyName];
    [self.detailScrollView setContentSize:CGSizeMake(320*3, 190)];
    
    
    PFFile *imageFile = [self.product valueForKey:keyMainImage]; 
    if (imageFile) {
        
        [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            
            if (!error) {
                
                if (![self contentTypeForImageData:data]) {
                    data = [data decompressGZip];
                }
        [self.detailImage setImage:[UIImage imageWithData:data]];
                
            }else {
                NSLog(@"%@",[error description]);
            }
        }];


    }
    
    self.detailsTextView.text = [self.product valueForKey:keyDetails];
    self.contactTextView.text = [NSString stringWithFormat:@"Location: %@\r\nPhone: %@\r\nEmail: %@\r\nWeb: %@",[self.product valueForKey:keyStoreLocation],[self.product valueForKey:keyContact],[self.product valueForKey:keyEmail],[self.product valueForKey:keyWeb]];

    NSString *price = [NSString stringWithFormat:@"S$%@",[[self.product valueForKey:keyPrice] stringValue]];
    
    [self.detailSegmentedControl setTitle:price forSegmentAtIndex:2];
	[((AppDelegate *)[[UIApplication sharedApplication] delegate]) registerEvent:eventBrowsed];
}

- (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

- (void)viewDidUnload
{
    
    [self setDetailScrollView:nil];
    [self setDetailSegmentedControl:nil];
    [self setDetailImage:nil];
    [self setDetailsTextView:nil];
    [self setContactTextView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(IBAction)buyAction:(id)sender
{
    
}

- (IBAction)clickedDetailSegmentControl:(UISegmentedControl *)sender {
    
    [self.detailScrollView setContentOffset:CGPointMake(sender.selectedSegmentIndex*320, 0) animated:YES];
}


#pragma mark - Featured Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView==self.detailScrollView) {
        
        CGFloat pageWidth = scrollView.frame.size.width;
        
        int index = floorf((scrollView.contentOffset.x+pageWidth/2)/pageWidth);
        //      int index =floorf((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        self.detailSegmentedControl.selectedSegmentIndex = index;
    }
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"gotoPlaceOrder"]) {
        
        OrderPreferenceViewController *controller = (OrderPreferenceViewController*) segue.destinationViewController;
        controller.product = self.product;
        controller.detailViewController = self;
        
    }
    
}
@end
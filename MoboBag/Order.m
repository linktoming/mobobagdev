//
//  Order.m
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import "Order.h"

@implementation Order

@synthesize product = _product;
@synthesize isSelfCollection = _isSelfCollection;
@synthesize isDeliveryService = _isDeliveryService;
@synthesize isCashOnDeliver = _isCashOnDeliver;
@synthesize isPayByPaypal = _isPayByPaypal;
@synthesize totalAmount = _totalAmount;
@synthesize user = _user;
- (NSString *)description{
    
//    NSLog([self.product valueForKey:keyName]);
//    NSLog(self.isSelfCollection?@"Self Collection":@"Delivery Service");
//    NSLog(self.isCashOnDeliver?@"Cash On Deliver":@"Pay by Paypal/Card");
//    NSLog(@"%@",self.totalAmount);
//    NSLog([self.user description]);

    NSString *desc = [NSString stringWithFormat:@"Product Name: %@\r\nDelivery Preference: %@\r\nPayment Preference: %@\r\nTotal Amount(SGD): %@\r\n%@\r\nProduct Identifier: %@",[self.product valueForKey:keyName], self.isSelfCollection?@"Self Collection":@"Delivery Service",self.isCashOnDeliver?@"Cash On Deliver":@"Pay by Paypal/Card",self.totalAmount,[self.user description],[self.product valueForKey:keyObjectId]];
    
    
    return desc;
}
@end

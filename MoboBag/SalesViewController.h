//
//  SalesViewController.h
//  MoboBag
//
//  Created by Mingming Wang on 27/6/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Product.h"
#import "DetailViewController.h"
#import <Parse/Parse.h>
#import "Constants.h"
#import "EGORefreshTableHeaderView.h"
#import "AppDelegate.h"
#import "NSData+GZip.h"
@interface SalesViewController : BaseViewController<UIScrollViewDelegate,EGORefreshTableHeaderDelegate>{
    
    EGORefreshTableHeaderView *_refreshHeaderView;
	
	//  Reloading var should really be your tableviews datasource
	//  Putting it here for demo purposes 
	BOOL _reloading;
}
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIScrollView *featuredScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *featuredPageControl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *featuredIndicator;

@property (weak, nonatomic) IBOutlet UIScrollView *fashionFemaleScrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *fashionFemaleIndicator;

@property (weak, nonatomic) IBOutlet UIScrollView *fashionMaleScrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *fashionMaleIndicator;

@property (weak, nonatomic) IBOutlet UIScrollView *gadgetAndAccesoryScrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *gadgetAndAccessoriesIndicator;

@property (weak, nonatomic) IBOutlet UIScrollView *bookScrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *booksIndicator;

@property (weak, nonatomic) IBOutlet UIScrollView *othersScrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *othersIndicator;

@property (strong, nonatomic) NSMutableArray *featuredProductArray;
@property (strong, nonatomic) NSMutableArray *fashionFProductArray;
@property (strong, nonatomic) NSMutableArray *fashionMProductArray;
@property (strong, nonatomic) NSMutableArray *gadgetAndAccesoriesProductArray;
@property (strong, nonatomic) NSMutableArray *booksProductArray;
@property (strong, nonatomic) NSMutableArray *otherProductArray;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *featuredProductButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *fashionFProductButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *fashionMProductButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *gadgetAndAccesoriesProductButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *booksProductButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *otherProductButtons;

- (IBAction)clickOnProduct:(UIButton *)sender;

@end

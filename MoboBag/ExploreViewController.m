//
//  ExploreViewController.m
//  MoboBag
//
//  Created by Mingming Wang on 5/7/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import "ExploreViewController.h"

@interface ExploreViewController ()

@end

@implementation ExploreViewController
@synthesize categoryTableView;
@synthesize categoryArray;
@synthesize categoryKeywordArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = YES;
    
}

- (void) viewWillDisappear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.categoryArray = [[NSArray alloc] initWithObjects:@"Fashion (F)",@"Fashion (M)",@"Gadgets and Accessories",@"Books",@"Others", nil];
    self.categoryKeywordArray = [[NSArray alloc] initWithObjects:categoryFashionF,categoryFashionM,categoryGadgetAndAccessories,categoryBooks,categoryOthers, nil];
    
}

- (void)viewDidUnload
{
    [self setCategoryTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark - table view delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.categoryTableView) {
        return 5;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.categoryTableView) {
        
        static NSString *CellIdentifier = @"productCategory";
        
        UITableViewCell *cell = [self.categoryTableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell.textLabel.text = [self.categoryArray objectAtIndex:[indexPath row]];
        
        NSString *imageName = [NSString stringWithFormat:@"featured%d.png", [indexPath row]+1];
        cell.imageView.image = [UIImage imageNamed:imageName];
        
        return cell;
        
    }
    return nil;
  
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"gotoProductList"]) {
        
        ProductListViewController *productListViewController = [segue destinationViewController];
        
        productListViewController.category = [self.categoryKeywordArray objectAtIndex:self.categoryTableView.indexPathForSelectedRow.row];
        productListViewController.navigationItem.title = [self.categoryArray objectAtIndex:self.categoryTableView.indexPathForSelectedRow.row];

        [self.categoryTableView deselectRowAtIndexPath:self.categoryTableView.indexPathForSelectedRow animated:YES];

        
    }
}
@end

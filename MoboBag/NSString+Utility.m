//
//  NSString+Utility.m
//  MoboBag
//
//  Created by Mingming Wang on 11/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)
-(BOOL) isEmpty{
    
    NSString *string = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return string.length == 0;
    
}
@end

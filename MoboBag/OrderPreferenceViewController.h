//
//  OrderPreferenceViewController.h
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "Constants.h"
#import <Parse/Parse.h>
#import "PlaceOrderViewController.h"
@interface OrderPreferenceViewController : UIViewController

@property (nonatomic,retain) Order *order;
@property (nonatomic,retain) PFObject *product;
@property (weak, nonatomic) IBOutlet UISwitch *isSelfCollection;
@property (weak, nonatomic) IBOutlet UISwitch *isDeliveryService;
@property (weak, nonatomic) IBOutlet UISwitch *isCashOnDelivery;
@property (weak, nonatomic) IBOutlet UISwitch *isPayByPaypal;
@property (weak, nonatomic) IBOutlet UILabel *totalAmount;
@property (weak, nonatomic) UIViewController * detailViewController;

- (IBAction)changedDeliveryPreference:(UISwitch *)sender;
- (IBAction)changedSelfCollection:(UISwitch *)sender;
- (IBAction)changedCashOnDelivery:(UISwitch *)sender;
- (IBAction)changedPayByPaypal:(UISwitch *)sender;

@end

//
//  SalesViewController.m
//  MoboBag
//
//  Created by Mingming Wang on 27/6/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import "SalesViewController.h"

@interface SalesViewController ()
- (void) getPromotionProductInCategory: (NSString *)category;
- (void) getFeaturedPromotionProduct;
- (void)startAnimationForCategory:(NSString *)category;
- (void)stopAnimationForCategory:(NSString *)category;
- (NSArray *) getButtonsForCategory: (NSString *)category;
-(void) setProductArrayForCategory: (NSString *)category withObjects:(NSArray *)objects;
-(BOOL)dataIsValidJPEG:(NSData *)data;
@end

@implementation SalesViewController
@synthesize mainScrollView;
@synthesize featuredScrollView,featuredPageControl,featuredIndicator,featuredProductButtons,featuredProductArray;
@synthesize fashionFemaleScrollView,fashionFemaleIndicator,fashionFProductButtons,fashionFProductArray;
@synthesize fashionMaleScrollView,fashionMaleIndicator,fashionMProductButtons,fashionMProductArray;
@synthesize gadgetAndAccesoryScrollView,gadgetAndAccessoriesIndicator,gadgetAndAccesoriesProductButtons,gadgetAndAccesoriesProductArray;
@synthesize bookScrollView,booksIndicator,booksProductButtons,booksProductArray;
@synthesize othersScrollView,othersIndicator,otherProductButtons,otherProductArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.mainScrollView.contentSize = CGSizeMake(320, 750);
    self.featuredScrollView.contentSize = CGSizeMake(320*5,130);
    self.fashionFemaleScrollView.contentSize = CGSizeMake(122*10, 73);
    self.fashionMaleScrollView.contentSize = CGSizeMake(122*10, 73);
    self.gadgetAndAccesoryScrollView.contentSize = CGSizeMake(122*10, 73);
    self.bookScrollView.contentSize = CGSizeMake(122*10, 73);
    self.othersScrollView.contentSize = CGSizeMake(122*10, 73);
    
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.mainScrollView.bounds.size.height, self.view.frame.size.width, self.mainScrollView.bounds.size.height)];
		view.delegate = self;
		[self.mainScrollView addSubview:view];
		_refreshHeaderView = view;
		
	}
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];
    [PFQuery clearAllCachedResults];
    
}

- (void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if(!self.featuredProductArray){
        [self getFeaturedPromotionProduct];
    }
    if (!self.fashionFProductArray) {
        [self getPromotionProductInCategory:categoryFashionF];
    }
    if (!self.fashionMProductArray) {
        [self getPromotionProductInCategory:categoryFashionM];
    }
    if (!self.gadgetAndAccesoriesProductArray) {
        [self getPromotionProductInCategory:categoryGadgetAndAccessories];
    }
    if (!self.booksProductArray) {
        [self getPromotionProductInCategory:categoryBooks];
    }
    if (!self.otherProductArray) {
        [self getPromotionProductInCategory:categoryOthers];
    }

}


-(void) getFeaturedPromotionProduct{

    [self.featuredIndicator startAnimating];
    self.featuredScrollView.hidden = YES;
    
    PFQuery *query = [PFQuery queryWithClassName:classProduct];
    [query whereKey:keyIsFeatured equalTo:[NSNumber numberWithBool:YES]];
    [query orderByAscending:keyDisplaySequence];
    query.cachePolicy=kPFCachePolicyCacheThenNetwork;
    query.limit = 5;
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d products in featured.", objects.count);
            
            for (UIButton *button in self.featuredProductButtons) {
                
                if (button.tag<[objects count]) {
                    
                    PFFile *imageFile = [[objects objectAtIndex:button.tag] valueForKey:keyMainImage];                    
                   
                    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                        
                        if (!error) {
                            
                            if (![self contentTypeForImageData:data]) {
                                data = [data decompressGZip];
                            }
                            [button setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
                            
                        }else {
                            NSLog(@"%@",[error description]);
                        }
                    }];
                    
                }
            }
            
            [self.featuredIndicator stopAnimating];
            self.featuredScrollView.hidden = NO;
            
            self.featuredProductArray = [[NSMutableArray alloc] initWithArray:objects];
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            self.featuredProductArray = nil;
        }
     [self doneLoadingTableViewData];
    }];
    

}


-(void) getPromotionProductInCategory: (NSString *)category{
    
    [self startAnimationForCategory:category];
    
    PFQuery *query = [PFQuery queryWithClassName:classProduct];
    [query whereKey:keyCategory equalTo:category];
    [query orderByDescending:keyUpdatedAt];
    query.cachePolicy=kPFCachePolicyCacheThenNetwork;
    query.limit = 10;
    
    NSArray *buttonsToSet = [self getButtonsForCategory:category];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d products in %@", objects.count,category);
            
            for (UIButton *button in buttonsToSet) {
                button.frame = CGRectMake(0, 0, 120, 73);
                if (button.tag<[objects count]) {

                    PFFile *imageFile = (PFFile *)[[objects objectAtIndex:button.tag] valueForKey:keyMainImage]; 

                    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                        
                        if (!error) {
                     
                            if (![self contentTypeForImageData:data]) {
                                data = [data decompressGZip];
                            }
                            [button setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
                            
                        }else {
                            NSLog(@"%@",[error description]);
                        }
                    }];
                    
                }
            }
            
            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationCurveEaseIn animations:^{
                
                for (UIButton *button in buttonsToSet) {
                    
                    button.frame = CGRectMake(button.tag*120+2*(button.tag), 0, 120, 73);
                }
            } completion:nil];
                        
            [self stopAnimationForCategory:category];
            
            [self setProductArrayForCategory:category withObjects:objects];

            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            [self setProductArrayForCategory:category withObjects:nil];
        }
    }];
    
}

- (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

-(BOOL)dataIsValidJPEG:(NSData *)data
{
    if (!data || data.length < 2) return NO;
    
    NSInteger totalBytes = data.length;
    const char *bytes = (const char*)[data bytes];
    
    return (bytes[0] == (char)0xff && 
            bytes[1] == (char)0xd8 &&
            bytes[totalBytes-2] == (char)0xff &&
            bytes[totalBytes-1] == (char)0xd9);
}

#pragma mark - Featured Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView==self.featuredScrollView) {
        
        CGFloat pageWidth = scrollView.frame.size.width;
        
        int index = floorf((scrollView.contentOffset.x+pageWidth/2)/pageWidth);
        //      int index =floorf((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        self.featuredPageControl.currentPage = index;
    }
    if (scrollView == self.mainScrollView) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
}

#pragma mark - Segue Related Method
- (IBAction)clickOnProduct:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"pushToDetails" sender:sender];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    
    if ([segue.identifier isEqualToString:@"pushToDetails"]) {
        
        NSArray *productArray;
        if ([self.featuredProductButtons containsObject:button]) {
            
            productArray = self.featuredProductArray;
            
        }else if ([self.fashionFProductButtons containsObject:button]){
            
            productArray = self.fashionFProductArray;
            
        }else if ([self.fashionMProductButtons containsObject:button]){
            
            productArray = self.fashionMProductArray;
            
        }else if ([self.gadgetAndAccesoriesProductButtons containsObject:button]){
            
            productArray = self.gadgetAndAccesoriesProductArray;
            
        }else if ([self.booksProductButtons containsObject:button]){
            
            productArray = self.booksProductArray;
            
        }else if ([self.otherProductButtons containsObject:button]){
            
            productArray = self.otherProductArray;
        }
        
        if ([productArray count]>button.tag) {
            
            PFObject *product = [productArray objectAtIndex:button.tag];

             ((DetailViewController *)segue.destinationViewController).product = product;
        }
    
    }
        
}

#pragma mark - private helper methods
- (void)startAnimationForCategory:(NSString *)category {
    if ([category isEqualToString:categoryFashionF]) {
        
        [self.fashionFemaleIndicator startAnimating];
        self.fashionFemaleScrollView.hidden = YES;
        
    }else if([category isEqualToString:categoryFashionM]) {
        
        [self.fashionMaleIndicator startAnimating];
        self.fashionMaleScrollView.hidden = YES;
        
    }else if([category isEqualToString:categoryGadgetAndAccessories]) {
        
        [self.gadgetAndAccessoriesIndicator startAnimating];
        self.gadgetAndAccesoryScrollView.hidden = YES;
        
    }else if([category isEqualToString:categoryBooks]) {
        
        [self.booksIndicator startAnimating];
        self.bookScrollView.hidden = YES;
        
    }else if([category isEqualToString:categoryOthers]) {
        
        [self.othersIndicator startAnimating];
        self.othersScrollView.hidden = YES;
    }
}

- (void)stopAnimationForCategory:(NSString *)category{
    if ([category isEqualToString:categoryFashionF]) {
        
        [self.fashionFemaleIndicator stopAnimating];
        self.fashionFemaleScrollView.hidden = NO;
        
    }else if([category isEqualToString:categoryFashionM]) {
        
        [self.fashionMaleIndicator stopAnimating];
        self.fashionMaleScrollView.hidden = NO;
        
    }else if([category isEqualToString:categoryGadgetAndAccessories]) {
        
        [self.gadgetAndAccessoriesIndicator stopAnimating];
        self.gadgetAndAccesoryScrollView.hidden = NO;
        
    }else if([category isEqualToString:categoryBooks]) {
        
        [self.booksIndicator stopAnimating];
        self.bookScrollView.hidden = NO;
        
    }else if([category isEqualToString:categoryOthers]) {
        
        [self.othersIndicator stopAnimating];
        self.othersScrollView.hidden = NO;
    }
}

-(NSArray *) getButtonsForCategory: (NSString *)category{
    
    if ([category isEqualToString:categoryFashionF]) {
        
        return self.fashionFProductButtons;
        
    }else if([category isEqualToString:categoryFashionM]) {
        
        return self.fashionMProductButtons;
        
    }else if([category isEqualToString:categoryGadgetAndAccessories]) {
        
        return self.gadgetAndAccesoriesProductButtons;
        
    }else if([category isEqualToString:categoryBooks]) {
        
        return self.booksProductButtons;
        
    }else if([category isEqualToString:categoryOthers]) {
        
        return self.otherProductButtons;
    }
    return nil;
    
}

-(void) setProductArrayForCategory: (NSString *)category withObjects:(NSArray *)objects{

    if ([category isEqualToString:categoryFashionF]) {
        
        self.fashionFProductArray = [[NSMutableArray alloc] initWithArray:objects];
        
    }else if([category isEqualToString:categoryFashionM]) {
        
         self.fashionMProductArray = [[NSMutableArray alloc] initWithArray:objects];
        
    }else if([category isEqualToString:categoryGadgetAndAccessories]) {
        
         self.gadgetAndAccesoriesProductArray = [[NSMutableArray alloc] initWithArray:objects];
        
    }else if([category isEqualToString:categoryBooks]) {
        
         self.booksProductArray = [[NSMutableArray alloc] initWithArray:objects];
        
    }else if([category isEqualToString:categoryOthers]) {
        
        self.otherProductArray = [[NSMutableArray alloc] initWithArray:objects];
    }

}
#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
    [self getFeaturedPromotionProduct];
    [self getPromotionProductInCategory:categoryFashionF];
    [self getPromotionProductInCategory:categoryFashionM];
    [self getPromotionProductInCategory:categoryGadgetAndAccessories];
    [self getPromotionProductInCategory:categoryBooks];
    [self getPromotionProductInCategory:categoryOthers];
	_reloading = YES;
	
}

- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.mainScrollView];
	
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
    if (scrollView == self.mainScrollView) {
        
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }

	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
	
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}


#pragma mark - 
- (void)viewDidUnload
{
    [self setMainScrollView:nil];
    [self setFeaturedScrollView:nil];
    [self setFeaturedPageControl:nil];
    [self setFashionFemaleScrollView:nil];
    [self setFashionMaleScrollView:nil];
    [self setGadgetAndAccesoryScrollView:nil];
    [self setBookScrollView:nil];
    [self setOthersScrollView:nil];
    [self setFeaturedProductButtons:nil];
    [self setFeaturedIndicator:nil];
    [self setFashionFemaleIndicator:nil];
    [self setFashionMaleIndicator:nil];
    [self setGadgetAndAccessoriesIndicator:nil];
    [self setBooksIndicator:nil];
    [self setOthersIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

@end

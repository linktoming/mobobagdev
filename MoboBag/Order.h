//
//  Order.h
//  MoboBag
//
//  Created by Mingming Wang on 10/7/12.
//  Copyright (c) 2012 moboleague.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Constants.h"
#import <Parse/Parse.h>
@interface Order : NSObject

@property (nonatomic,retain) PFObject *product;
@property (nonatomic,assign) BOOL  isSelfCollection;
@property (nonatomic,assign) BOOL  isDeliveryService;
@property (nonatomic,assign) BOOL  isCashOnDeliver;
@property (nonatomic,assign) BOOL  isPayByPaypal;
@property (nonatomic,retain) NSNumber *totalAmount;
@property (nonatomic,retain) User *user;


@end

//
//  BaseViewController.h
//  MoboBag
//
//  Created by Mingming Wang on 5/7/12.
//  Copyright (c) 2012 linktoming.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
@interface BaseViewController : UIViewController

@end
